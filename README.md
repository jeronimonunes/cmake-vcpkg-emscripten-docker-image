# Cmake Vcpkg Emscripten Docker Image

This repo aims to provide a Dockerfile to build an image that provides cmake, vcpkg, emscripten and g++.
The image will be useful when people wants to compile something to WebAssembly or linux binaries and use vcpkg dependencies