FROM ubuntu

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
    cmake \
    curl \
    g++ \
    git \
    python \
    tar \
    unzip

RUN git clone https://github.com/Microsoft/vcpkg.git && \
    /vcpkg/bootstrap-vcpkg.sh

RUN git clone https://github.com/emscripten-core/emsdk.git && \
    /emsdk/emsdk install latest && \
    /emsdk/emsdk activate latest

COPY x86-wasm.cmake /vcpkg/triplets

COPY Emscripten.cmake /emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake